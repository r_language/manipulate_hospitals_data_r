**Project with 6 files and 7 questions.**

- File 1 codegeo_geoloc with 5795 rows containing code_geo, longitude and latitude columns. 

- File 2 finess_geolocalisation with 9412 rows containing finess, finessJ, finess_rs, adresse, code_postal, ville, x, y, longitude and latitude columns.

- File 3 finess with 94943 rows containing finess_et, finess_ej, raison_sociale, raison_sociale_longue, departement, lib_departement, code_commune and code_postal columns.

- File 4 finess_geo with 46924 rows containing finessGeo, ides, finess_pmsi, finessJ, annee, categ_geo, categ_detail_geo, categ_pmsi, secteur_pmsi, rs, mco, ssr, psy, had, mco_ace, mco_smur, code_com, niveau_pmsi columns.

- File 5 laposte with 39192 rows containing Code_commune_INSEE, Nom_commune, Code_postal, Libellé_d_acheminement and coordonnees_gps columns.

- File 6 with 7069192 rows containing NIVGEO, CODGEO, LIBGEO, SEXE, AGED100, NB.

**Question 1 :** 
Datamanagement.

**Question 2 :** 
Create a dataframe that contains all hospitals in France. Create another dataframe that contains all university hospitals in France. And create another dataframe that contains all hospitals in four specific departments (Nord, Pas de Calais, Aisne and Somme).

**Question 3 :** 
Calculates the distance between Santes city and the public university hospital CHU de Lille Hôpital Salengro.

**Question 4 :** 
Identify the shortest distance between each towns and each hospitals in France. After, realize some descriptive and graphical statistics.

**Question 5 :** 
Identify the shortest distance between each towns and each university hospitals in France. After, realize some descriptive and graphical statistics.
Question 6 : Identify the shortest distance between each towns and each hospitals but also each university hospitals in France for the population in the department Nord. After, realize some descriptive and graphical statistics.
Question 7 : Show some descriptive and graphical statistics concerning people who are more than 75 years old in France in each department. Same for people living at more than 25Km and more than 50Km having more or less than 75 years old.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

December 2020


